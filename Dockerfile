FROM python:3

ARG YAMLLINT_VERSION=1.26.1


COPY config/ /home/config/
RUN pip install --upgrade pip \
	&& pip install --no-cache --upgrade pip \
	&& pip install --no-cache requests yamllint==${YAMLLINT_VERSION} anybadge; \
	chmod +x /home/config/start.sh; \
	chmod +x /home/config/badge.py;

WORKDIR /home/project
ENTRYPOINT ["/home/config/start.sh"]
