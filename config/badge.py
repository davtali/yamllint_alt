#!/usr/local/bin/python
                                                                                                                                                                                    
import os                                                                                                                                                                           
import requests                                                                                                                                                                     
import sys                                                                                                                                                                          


def main():
    '''
    initialize badge yamllint if no exist already
    if exist, do an update link
    '''
    header={"PRIVATE-TOKEN": os.environ['TOKEN']}
    url=os.environ['CI_API_V4_URL']+"/projects/"+os.environ['CI_PROJECT_ID']+"/badges/"
    rep=requests.get(url=url, headers=header).json()


    # check if project have already badge warning or failure
    failure=0
    warning=0
    for badge in rep:
        if badge['name']=='yamllint_warning':warning=badge['id']
        elif badge['name']=='yamllint_failure':failure=badge['id']
        if failure and warning: break
    

    # add badge if no exist
    if not warning:
        dico={
            "name":"yamllint_warning",
            "image_url":os.environ['CI_PROJECT_URL']+"/-/jobs/artifacts/"+os.environ['CI_DEFAULT_BRANCH']+"/raw/yaml_warning.svg?job="+os.environ['CI_JOB_NAME'],
            "link_url":os.environ['CI_JOB_URL']
            }
        requests.post(url=url, headers=header, data=dico)
    # edit badge if exist
    else:
        dico={
            "link_url":os.environ['CI_JOB_URL']
            }
        requests.put(url=url+str(warning), headers=header, data=dico)



    if not failure:
        dico={
            "name":"yamllint_failure",
            "image_url":os.environ['CI_PROJECT_URL']+"/-/jobs/artifacts/"+os.environ['CI_DEFAULT_BRANCH']+"/raw/yaml_failure.svg?job="+os.environ['CI_JOB_NAME'],
            "link_url":os.environ['CI_JOB_URL']
            }
        requests.post(url=url, headers=header, data=dico)

    else:
        dico={
            "link_url":os.environ['CI_JOB_URL']
            }
        requests.put(url=url+str(failure), headers=header, data=dico)



if __name__ == "__main__":
    main()