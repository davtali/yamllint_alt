#!/bin/bash


warning=0
c_warning="green"
failure=0
c_failure="green"

yamllint  -f colored -d "{extends: default, rules: {$exclude}, ignore: .gitlab-ci.yml}" . 1 > err.txt


if [[ "$?" == "0" ]]
then
    state="0"
else 
    state="1"
    err=$(cat err.txt)
    echo "$err"
    error=$(echo "$err" | grep "error" | wc -l)
    warning=$(echo "$err" | grep "warning" | wc -l)

    if (( $failure>0 )); then
    c_failure="brightred"
    fi

    if (( $warning>0 )); then
    c_warning="brightred"
    fi

fi


NC='\033[0m'
RED='\033[0;31m'
YELLOW='\033[0;33m'
LBLUE='\033[1;34m'
echo -e "\n${LBLUE}$error ${RED}failures${NC}, ${LBLUE}$warning ${YELLOW}warnings${NC}\n"


anybadge -l yaml_failure -v $failure -f yaml_failure.svg -c $c_failure
anybadge -l yaml_warning -v $warning -f yaml_warning.svg -c $c_warning


# upload or update badge
/home/config/badge.py