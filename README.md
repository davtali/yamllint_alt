# yamllint_alt
![](https://gitlab.com/davtali/ansible-mongodb/-/jobs/artifacts/main/raw/yaml_warning.svg?job=ansible-lint_alt) ![](https://gitlab.com/davtali/ansible-mongodb/-/jobs/artifacts/main/raw/yaml_failure.svg?job=ansible-lint_alt) \
Was make by yamllint 1.26.1 

Ansible-lint alternative version \
This version create badge dynamic with anybadge, the badge print the number of warning and failure detected by yamllint \
The image was stored on artefact

The lint give your the best bratice for project who use YAML syntax. \
The lint was applied on full project \
The badge redirect to the last job where we can see the result lint on terminal

You can show example on the project: [here](https://gitlab.com/davtali/ansible-mongodb)

For show an alternative version to add badge yamllint without external library: [here](https://gitlab.com/davtali/yamllint)

For add badge ansible: [here](https://gitlab.com/davtali/ansible-lint_alt)


# Quick start

```
stages:
    - lint

yamllint_alt:
  stage: lint
  image:
    name: dal3ap1/yamllint_badge:latest
    entrypoint: [""]
  variables:
  script:
    - /home/config/start.sh
  artifacts:
    paths:
      - yaml_failure.svg
      - yaml_warning.svg
    expire_in: 3 yrs
```

You can specify the rules who you want ignore it on lint

```
yamllint_alt:
  stage: lint
  image:
    name: dal3ap1/yamllint_badge:latest
    entrypoint: [""]
  variables:
    exclude: "line-length: disable,
              comments: disable, 
              empty-lines: disable"
  script:
    - /project/config/start.sh
```
for check all the rules show documentation yamllint.


